/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* main.c - the backbone of the program */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <limits.h>
#include <locale.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if ENABLE_TIME
# include <time.h>
#endif /* ENABLE_TIME */

#include <gop.h>

#if ENABLE_NLS
# include "propername.h"
#else /* !ENABLE_NLS */
# define proper_name_utf8(name_ascii, name_utf8) (name_utf8)
#endif /* !ENABLE_NLS */

#include "asciidoc.h"
#include "command.h"
#include "file-type.h"
#include "main.h"
#include "nls.h"
#include "output.h"
#include "parser.h"
#include "print-error.h"
#include "strip.h"
#include "test.h"
#include "xalloc.h"

/* The default tabsize. It is used to calculate the column where the
 * content of a comment begins, that is where a command and tag might be
 * found. */
#define DEFAULT_TABSIZE 4

void __attribute__((nonnull))
get_meta(const cotesdex_t * const cotesdex,
         char ** const title,
         char ** const description,
         char ** const license)
{
    /* NULL the fields. */
    *title = NULL;
    *description = NULL;
    *license = NULL;

    const command_t * const root = cotesdex->commands;
    command_t * command = command_get_child(root);
    while (command != NULL &&
           (*title == NULL || *description == NULL || *license == NULL))
    {
        switch (command_get_type(command)) {
            case COMMAND_TITLE:
                if (*title == NULL) {
                    *title = command_harvest_argument(command);
                }
                command = command_unlink(command);
                break;
            case COMMAND_DESCRIPTION:
                if (*description == NULL) {
                    *description = command_harvest_content(command);
                }
                command = command_unlink(command);
                break;
            case COMMAND_LICENSE:
                if (*license == NULL) {
                    *license = command_harvest_content(command);
                }
                command = command_unlink(command);
                break;
            default:
                command = command_get_sister(command);
                break;
        }
        
    }

    return;
}

/* This function will find out base path of the current file and save it
 * to the base_path member of the cotesdex structure. It is calculated
 * like this:
 * 1. If the user has given the --base-path use it.
 * 2. If the input file name is not NULL the directory where the file
 *    resides is used.
 * 3. If the input is read from standard in use the current working
 *    directory.
 * The function only needs to be called once.
 * The saved path will not contain a trailing slash.
 * The function returns 0 on success and 1 on failure. */
static int __attribute__((nonnull(1)))
get_base_path(cotesdex_t * const cotesdex, const char * const base_path)
{
    if (base_path != NULL) {
        cotesdex->base_path = realpath(base_path, NULL);
        if (cotesdex->base_path == NULL) {
            print_error_errno(_("realpath() failed for %s"), base_path);
            return 1;
        }
    } else if (cotesdex->input_file_name != NULL) {
        cotesdex->base_path = realpath(cotesdex->input_file_name, NULL);
        if (cotesdex->base_path == NULL) {
            print_error_errno(_("realpath() failed for %s"), 
                              cotesdex->input_file_name);
            return 1;
        }

        /* Remove the last component of the path (the file name) so that
         * only the directory is included. */
        char * const end = strrchr(cotesdex->base_path, '/');
        assert(end != NULL);
        *end = '\0';
    } else {
        /* Although, the result of getcwd(NULL, 0) is not defined in POSIX, this
         * call can safely be used since gnulib is being used in cases where
         * getcwd(NULL, 0) does not work. */
        cotesdex->base_path = getcwd(NULL, 0);
        if (cotesdex->base_path == NULL) {
            print_error_errno(_("could not get current working directory"));
            goto error;
        }
    }

    const size_t len = strlen(cotesdex->base_path);
    cotesdex->base_path = xrealloc(cotesdex->base_path, len + 1);

    /* The function description requires that the path name does not end
     * with a slash. Make sure that always holds true. */
    assert(cotesdex->base_path[len-1] != '/');

    return 0;
error:
    free(cotesdex->base_path);
    cotesdex->base_path = NULL;
    return 1;
}

/* This function removes
 * (1) all leading line breaks,
 * (2) one of the line breaks in a sequence of four line breaks and
 * (3) all trailing line breaks
 * from the code.
 * 
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
remove_bad_line_breaks(cotesdex_t * const cotesdex)
{
    char * c = cotesdex->code;
    size_t count = 0;

    /* Remove all leading line breaks. */
    while (*c == '\n') ++c;
    count = (size_t)(c - cotesdex->code);
    cotesdex->code_len -= count;
    memmove(cotesdex->code, c, cotesdex->code_len);

#define MAX_N_LINE_BREAK 3

    if (cotesdex->code_len > 1) {
        /* Remove double line breaks in the code. */
        /* +1 because line breaks have already been removed from the
         * beginning. */
        char * dest = cotesdex->code + 1;
        while (*dest != '\0') {
            while (*dest != '\n' && *dest != '\0') ++dest;
            if (*dest == '\0') break;
            /* +1 because there is already a line break at dest. */
            c = dest + 1;
            while (*c == '\n') ++c;
            count = (size_t)(c - dest);
            if (count >= MAX_N_LINE_BREAK) {
                memmove(dest + MAX_N_LINE_BREAK - 1, c,
                        cotesdex->code_len - (size_t)(c - cotesdex->code));
                cotesdex->code_len -= count - MAX_N_LINE_BREAK + 1;
            }
            ++dest;
        }

        /* Remove trailing line breaks. */
        while (cotesdex->code[cotesdex->code_len-1] == '\n') {
            cotesdex->code_len--;
        }
    }

    cotesdex->code = xrealloc(cotesdex->code, cotesdex->code_len + 1);
    cotesdex->code[cotesdex->code_len] = '\0';

    return 0;
}

static gop_return_t
print_version(gop_t * const gop)
{
    printf(_("%s %s\n"
             "Copyright (C) 2013-2015 %s\n"
             "License GPLv3+: GNU GPL version 3 or later\n"
             "<http://gnu.org/licenses/gpl.html>\n"
             "This is free software: you are free to change and redistribute "
             "it.\n"
             "There is NO WARRANTY, to the extent permitted by law.\n"),
             PACKAGE, PACKAGE_VERSION,
             /* TRANSLATORS: This is a proper name.  See the gettext manual,
              * section Names.  Note this is actually a non-ASCII name: The last
              * name is (with Unicode escapes) "Lind\u00e9n". Pronunciation is
              * like "lind-een".  */
             proper_name_utf8("Karl Linden", "Karl Lind\xc3\xa9n"));
    return GOP_DO_EXIT;
}

static gop_return_t __attribute__((nonnull))
aterror_callback(gop_t * const gop)
{
    const gop_error_t error = gop_get_error(gop);
    switch (error) {
        /* These errors are all parsing errors, that can be fixed if the
         * user inputs the correct options and arguments, so advice the
         * user to look at the --help output. */
        case GOP_ERROR_EXPARG:
        case GOP_ERROR_UNEXARG:
        case GOP_ERROR_UNKLOPT:
        case GOP_ERROR_UNKSOPT:
            goto default_error_handler;

        /* The default is to use the default error handler of GOP. */ 
        default:
            return GOP_DO_CONTINUE;
    }

default_error_handler:
    /* The default error handler of GOP will print a neat error. */
    gop_default_error_handler(gop);

    /* Advice the user to look at the --help output. */
    print_error("use --help to see available options");

    /* The program should exit. */
    gop_set_exit_status(gop, EXIT_FAILURE);
    return GOP_DO_EXIT;
}

/* This function initializes the program structure. */
static void __attribute__((nonnull))
cotesdex_init(cotesdex_t * const cotesdex)
{
    cotesdex->code = NULL;
    /* The code_len member does not need to be set because the program
     * will stop before reading it if it was not filled in when the file
     * was parsed. */
    /* cotesdex->code_len = 0; */

    cotesdex->commands = command_root();

    cotesdex->output_file = stdout;
    cotesdex->output_comment = false;
    /* As an optimization the output_markup member is not set until
     * output_set_markup() is called. That means no calls to the
     * output_print() should be done before output_set_markup() has been
     * called. */
    /* cotesdex->output_markup = NULL */

    cotesdex->base_path = NULL;
    cotesdex->input_file_name = NULL;
    cotesdex->executable_file_name = NULL;
    cotesdex->file_type_string = NULL;
    cotesdex->here_string = NULL;
    cotesdex->output_file_name = NULL;

    cotesdex->memcheck = 0;
    cotesdex->valgrind = NULL;

    return;
}

/* This function free()s all used memory that is in use by the cotesdex
 * structure. */
static void __attribute__((nonnull))
cotesdex_destroy(cotesdex_t * const cotesdex)
{
    free(cotesdex->code);
    command_destroy_recursive(cotesdex->commands);

    output_close_file(cotesdex);

    free(cotesdex->base_path);
    free(cotesdex->input_file_name);
    free(cotesdex->executable_file_name);
    free(cotesdex->output_file_name);

    return;
}

int
main(int argc, char ** const argv)
{
    int return_value = EXIT_SUCCESS;

#if ENABLE_TIME
    int do_time = 0;
#endif /* ENABLE_TIME */

    int do_asciidoc = 0;
    int do_strip = 0;
    int do_test = 0;

    unsigned short tabsize = DEFAULT_TABSIZE;

    /* The program structure to be used. */
    cotesdex_t cotesdex;
    cotesdex_init(&cotesdex);

    char * base_path = NULL;
    char * executable_file_name = NULL;
    char * output_file_name = NULL;

    /* Some long and short options in this table conform to GNU software
     * to follow the GNU coding standard a bit better. For the list of
     * options see:
     * https://www.gnu.org/prep/standards/standards.html#Option-Table */
    const gop_option_t options[] = {
        {"asciidoc", 'a', GOP_NONE, &do_asciidoc, NULL,
            N_("Generate asciidoc output"), NULL},
        {"strip", 's', GOP_NONE, &do_strip, NULL,
            N_("Strip cotesdex markup"), NULL},
        {"test", 't', GOP_NONE, &do_test, NULL,
            N_("Generate a shell script test"), NULL},
        {"output", 'o', GOP_STRING, &output_file_name, NULL,
            N_("Specify output file to use"), N_("FILE")},
        {"executable", 'e', GOP_STRING, &executable_file_name,
            NULL, N_("Specify executable to test"), N_("EXECUTABLE")},
        {"base-path", 'b', GOP_STRING, &base_path, NULL,
            N_("Specify base path"), N_("BASE_PATH")},
        {"here-string", 'h', GOP_STRING, &cotesdex.here_string, NULL,
            N_("Specify here-string to used in the generated test"),
            N_("HERE_STRING")},

        /* --file-type and -F conform to GNU ls. */
        {"file-type", 'F', GOP_STRING, &cotesdex.file_type_string, NULL,
            N_("Specify file type of the input (one of autodetect, c and "
               "raw)"),
            N_("FILE_TYPE")},

        /* --tabsize and -T conform to GNU ls and GNU nano. */
        {"tabsize", 'T', GOP_UNSIGNED_SHORT, &tabsize, NULL,
            N_("Assume tab stops at each COLS instead of 4"), N_("COLS")},

#if ENABLE_TIME
        {"time", '\0', GOP_NONE, &do_time, NULL,
            N_("Measure and print the time it takes to generate the output"),
            NULL},
#endif /* ENABLE_TIME */

        {"version", 'v', GOP_NONE, NULL, &print_version,
            N_("Show version information and exit"), NULL},
        GOP_TABLEEND
    };

    const gop_option_t valgrind_options[] = {
        {"memcheck", '\0', GOP_NONE, &cotesdex.memcheck, NULL,
            N_("Run tests in valgrind's memcheck tool"), NULL},
        {"valgrind", '\0', GOP_STRING, &cotesdex.valgrind, NULL,
            N_("The valgrind program to use"), N_("VALGRIND")},
        GOP_TABLEEND
    };

#if ENABLE_NLS
    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);
    gop_init_nls();
#endif /* ENABLE_NLS */

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    /* Attach the error handler. */
    gop_aterror(gop, aterror_callback);

    gop_add_usage(gop,
                  _("{-a|--asciidoc|-s|--strip|-t|--test} [OPTION...] [FILE]"));

    gop_description(gop,
                    _("Create a test, a piece of documentation, or an "
                      "example from a common input file."));

    gop_extra_help(gop,
                   _("If no input file is specified, standard in will be read. "
                     "If no output file is specified (using --output or -o), "
                     "output will be written to standard out\n"
                     "\n"
                     "Report bugs to: %s\n"
                     "%s home page: %s"),
                   PACKAGE_BUGREPORT, PACKAGE_NAME, PACKAGE_URL);

    /* Add the option tables. */
    gop_add_table(gop, _("Application options:"), options);
    gop_add_table(gop, _("Valgrind options:"), valgrind_options);
    gop_autohelp(gop);

    /* Parse options. */
    gop_parse(gop, &argc, argv);

    gop_destroy(gop);

    /* Make sure tabsize is valid (non-zero). */
    if (tabsize == 0) {
        print_error(_("tab size is not allowed to be zero (0)"));
        goto error;
    }

    if (do_asciidoc + do_strip + do_test > 1) {
        print_error(_("only one of `--asciidoc', `--strip' and `--test' may "
                      "be given at once"));
        goto error;
    } else if (!do_asciidoc && !do_strip && !do_test) {
        print_error(_("you must specify what to do"));
        print_error(_("use --help to see available options"));

        goto error;
    }

#if ENABLE_TIME
    /* ts1 is this measure point and ts2 is the second one (when
     * "everything" is done). */
    struct timespec ts1; 
    if (do_time) {
        if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts1)) {
            print_error_errno(_("clock_gettime failed"));
        }
    }
#endif /* ENABLE_TIME */

    if (argc == 2) {
        cotesdex.input_file_name = realpath(argv[1], NULL);
        if (cotesdex.input_file_name == NULL) {
            print_error(_("realpath() failed for %s"), argv[1]);
            goto error;
        }
    } else if (argc > 2) {
        print_error(_("too many arguments given"));
        goto error;
    }

    if (cotesdex.file_type_string == NULL ||
        strcmp(cotesdex.file_type_string, "autodetect") == 0)
    {
        cotesdex.input_file_type =
            file_type_autodetect(cotesdex.input_file_name);
    } else if (strcmp(cotesdex.file_type_string, "c") == 0) {
        cotesdex.input_file_type = FILE_TYPE_C;
    } else if (strcmp(cotesdex.file_type_string, "raw") == 0) {
        cotesdex.input_file_type = FILE_TYPE_RAW;
    } else {
        print_error(_("'%s' is an invalid file type"),
                    cotesdex.file_type_string);
        goto error;
    }

    /* Parse the input and fill in the data. */
    if (parser_process(&cotesdex, tabsize)) {
        goto error;
    }

    remove_bad_line_breaks(&cotesdex);

    if (output_file_name != NULL &&
        output_open_file(&cotesdex, output_file_name))
    {
        goto error;
    }

    /* Make sure the executable file name is absolute so that we can
     * chdir() without losing information about where it is. */
    if (executable_file_name != NULL) {
        cotesdex.executable_file_name = realpath(executable_file_name, NULL);
        if (cotesdex.executable_file_name == NULL) {
            print_error_errno(_("realpath() failed for %s"),
                              executable_file_name);
            goto error;
        }
    }

    /* The file generation should work from the base path. */
    if (get_base_path(&cotesdex, base_path)) {
        goto error;
    }
    if (chdir(cotesdex.base_path)) {
        print_error_errno(_("could not change directory"));
        goto error;
    }

    if (do_asciidoc) {
        if (asciidoc(&cotesdex)) goto error;
    } else if (do_strip) {
        if (strip(&cotesdex)) goto error;
    } else if (do_test) {
        if (test(&cotesdex)) goto error;
    }

#if ENABLE_TIME
    if (do_time) {
        struct timespec ts2;
        if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts2)) {
            print_error_errno(_("clock_gettime failed"));
        }
        long unsigned int nsecs =
            (ts2.tv_sec - ts1.tv_sec) * 1000000000 +
            (ts2.tv_nsec - ts1.tv_nsec);
        printf(_("%s: generated output in %ld nanoseconds\n"), PACKAGE,
               nsecs);
    }
#endif /* ENABLE_TIME */

    if (false) {
    error:
        return_value = EXIT_FAILURE;
    }

    cotesdex_destroy(&cotesdex);

    return return_value;
}
