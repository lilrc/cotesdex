/* 
 * This file is part of cotesdex.
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* print-error.h - error printing functions */

#ifndef PRINT_ERROR_H
# define PRINT_ERROR_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

void print_error(const char * const fmt, ...)
    __attribute__((cold, nonnull, format (printf, 1, 2)));

void print_error_errno(const char * const fmt, ...)
    __attribute__((cold, nonnull, format (printf, 1, 2)));

#endif /* !PRINT_ERROR_H */
