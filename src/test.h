/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* test.h - routines needed to generate a test script, header */

#ifndef TEST_H
# define TEST_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include "main.h"

/* Generates a test from the given data. All file names given to this
 * function must be absolute paths.
 * 
 * The here_string parameter is used for the here-strings in the
 * generated test scripts. It can be used as workaround when the
 * here-string is in the content. If here_string is NULL the default
 * will be used.
 * 
 * Returns 0 on success and 1 on failure. */
int test(cotesdex_t * const cotesdex)
    __attribute__((nonnull));

#endif /* !TEST_H */
