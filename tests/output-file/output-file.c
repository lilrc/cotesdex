/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title A simple demonstration of the output-file command */
/* @cotesdex description
 * This example is used to demonstrate (and test) the output-file
 * command of cotesdex. This example program will read from standard
 * input and replace all occurences of one string with another and
 * output the result to standard output.
 * 
 * Usage: output-file <find> <replace> [output-file]
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  Hello
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  Too many arguments given.
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  Hello Hi
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdin
 *   @cotesdex content
 *   Hello, my name is ...
 *  @cotesdex stdout
 *   @cotesdex content
 *   Hi, my name is ...
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  Hello Hi output.txt
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *  @cotesdex stdin
 *   @cotesdex content
 *   Hello, my name is ...
 *  @cotesdex output-file output.txt
 *   @cotesdex remove
 *   @cotesdex content
 *   Hi, my name is ...
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  "hot dogs" "boiled snails" output.txt
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *  @cotesdex stdin
 *   @cotesdex content
 *   I like hot dogs and I eat such every day. There is no other dish
 *   that I can eat in larger amounts but hot dogs. However, I find it
 *   strange that you call it hot dogs. Why not "hot cats"?
 *  @cotesdex output-file output.txt
 *   @cotesdex remove
 *   @cotesdex content
 *   I like boiled snails and I eat such every day. There is no other dish
 *   that I can eat in larger amounts but boiled snails. However, I find it
 *   strange that you call it boiled snails. Why not "hot cats"?
 * @cotesdex */

/* EOF, fclose(), fopen(), fprintf(), getchar(), fputc(), stderr */
#include <stdio.h>

/* EXIT_FAILURE, EXIT_SUCCESS */
#include <stdlib.h>

int
main (const int argc, const char * const * const argv)
{
    if (argc < 3 || argc > 4) {
        fprintf(stderr, "Insufficient number of arguments.\n"
                        "Usage: %s <find> <replace> [output-file]\n",
                        argv[0]);
        return EXIT_FAILURE;
    }

    const char * const find = argv[1];
    const char * const replace = argv[2];
    const char * const output_file_name = argv[3];

    FILE * output_file;
    if (output_file_name != NULL) {
        output_file = fopen(output_file_name, "w");
        if (output_file == NULL) {
            perror("fopen");
            return EXIT_FAILURE;
        }
    } else {
        output_file = stdout;
    }

    int c;
    size_t i = 0;
    while ((c = getchar()) != EOF) {
        if (c == find[i]) {
            ++i;
            if (find[i] == '\0') {
                for (const char * r = replace; *r != '\0'; ++r) {
                    fputc(*r, output_file);
                }
                i = 0;
            }
        } else {
            if (i > 0) {
                for (size_t j = 0; j < i; ++j) {
                    fputc(find[j], output_file);
                }
                i = 0;
            }
            fputc(c, output_file);
        }
    }

    if (fclose(output_file)) perror("fclose");
    return EXIT_SUCCESS;
}
