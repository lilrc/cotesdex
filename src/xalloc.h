/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* xalloc.h - safe memory allocation functions */

#ifndef XALLOC_H
# define XALLOC_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stdlib.h>

# include "nls.h"
# include "print-error.h"

/* These functions are inlined wrappers for the malloc() and realloc()
 * calls. Both functions are guaranteed to not return NULL on failure,
 * but instead exit the program. */
static inline void * __attribute__((alloc_size(1), returns_nonnull))
xmalloc(const size_t size)
{
    void * ptr = malloc(size);
    if (ptr == NULL) {
        print_error_errno(_("could not allocate memory"));
        exit(EXIT_FAILURE);
    }
    return ptr;
}

static inline void * __attribute__((alloc_size(2), returns_nonnull))
xrealloc(void * ptr, const size_t size)
{
    ptr = realloc(ptr, size);
    if (ptr == NULL) {
        print_error_errno(_("could not allocate memory"));
        exit(EXIT_FAILURE);
    }
    return ptr;
}

#endif /* !XALLOC_H */
