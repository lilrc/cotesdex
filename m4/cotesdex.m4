#  
#  This file is part of cotesdex.
#  
#  Copyright (C) 2015 Karl Linden <lilrc@users.sourceforge.net>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

AC_DEFUN_ONCE([_COTESDEX_FLAGS],
              [AC_ARG_VAR([COTESDEXFLAGS],
                          [Flags that should be passed to cotesdex])])
# COTESDEX_INIT
# -------------
AC_DEFUN_ONCE([COTESDEX_INIT],
              [AC_ARG_VAR([ASCIIDOC], [The asciidoc program])
              AC_ARG_VAR([COTESDEX], [The cotesdex program])
              _COTESDEX_FLAGS])

# COTESDEX_PROG_COTESDEX
# ----------------------
AC_DEFUN([COTESDEX_PROG_COTESDEX],
         [AC_REQUIRE([COTESDEX_INIT])
         AC_CHECK_PROGS([COTESDEX], [cotesdex])
         AS_IF([test x$COTESDEX = x],
               [AC_MSG_FAILURE(
[The cotesdex program is required, but cannot be found.
To get cotesdex, visit <https://bitbucket.org/lilrc/cotesdex>])])])

# COTESDEX_PROG_ASCIIDOC
# ----------------------
AC_DEFUN([COTESDEX_PROG_ASCIIDOC],
         [AC_REQUIRE([COTESDEX_INIT])
         AC_CHECK_PROGS([ASCIIDOC], [asciidoc])
         AS_IF([test x$ASCIIDOC = x],
               [AC_MSG_FAILURE(
[The asciidoc program is required to build documentation, but it cannot be
found.])])
         AC_CHECK_PROGS([SOURCE_HIGHLIGHT], [source-highlight])
         AS_IF([test x$SOURCE_HIGHLIGHT = x],
               [AC_MSG_FAILURE(
[The source-highlight program is required to build documentation, but it cannot
be found])])])
