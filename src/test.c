/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* test.c - routines needed to generate a test script */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "buffer-foreach-line.h"
#include "command.h"
#include "file-type.h"
#include "main.h"
#include "nls.h"
#include "output.h"
#include "print-error.h"
#include "test.h"

#define oprint(...) output_print(cotesdex, __VA_ARGS__)
#define oputs(string) output_puts(cotesdex, (string))

#define TEST_FILE_PREFIX "${test_tmpdir}/"
#define OUTPUT_SET_TEST_FILE(file_name) \
    oputs(file_name "=" TEST_FILE_PREFIX file_name "\n")

/* A dummy struct to supress the const warning. */
struct content_s {
    const cotesdex_t * cotesdex;
};

struct print_command_line_s {
    const cotesdex_t * cotesdex;
    bool had_line_before;
};

static inline void __attribute__((nonnull))
create_helpers(const cotesdex_t * const cotesdex)
{
    oputs("script=$(basename ${0})\n"
          "\n"
          "msg() {\n"
          "    echo \"${script}: ${@}\" > /dev/stderr\n"
          "}\n"
          "\n"
          "die() {\n"
          "    msg ${@}\n"
          "    exit 1\n"
          "}\n"
          "\n"
          "warn() {\n"
          "    msg \"warning: ${@}\"\n"
          "}\n"
          "\n");
    return;
}

static void __attribute__((nonnull))
content_line(const char * const line, void * argument)
{
    struct content_s * const cs = (struct content_s *)argument;
    output_print(cs->cotesdex, "%s\n", line);
    return;
}

/* This function creates a file in the current temporary directory
 * prefixed with the test prefix and fills it with the specified
 * content.
 * 
 * The variable_name string should not contain any messy characters that
 * cannot be in a variable name because it will break the script. If
 * there is a risk that such characters are in the string use the
 * create_file_with_content_safe function instead. */
static void __attribute__((nonnull))
create_file_with_content(const cotesdex_t * const cotesdex,
                         const char * file_name,
                         const char * variable_name,
                         const char * const content,
                         const char * const here_string)
{
    oprint("%s=" TEST_FILE_PREFIX "%s\n", variable_name, file_name);
    if (content[0] != '\0') {
        oprint("cat << '%s' > $%s\n", here_string, variable_name);
        struct content_s content_struct;
        content_struct.cotesdex = cotesdex;
        buffer_foreach_line(content, &content_line, &content_struct);
        oprint("%s\n", here_string);
    } else {
        oprint("echo -n > $%s\n", variable_name);
    }

    return;
}

/* This function will make a string suitable to be used as a file name,
 * i.e. it will replace all messy characters with underlines.
 * 
 * The string will be altered. */
static inline void __attribute__((nonnull))
make_safe(char * const string)
{
    const char messy[] = "-.";
    for (char * s = string; (s = strpbrk(s, messy)) != NULL; *s = '_')
    {
        ;
    }
    return;
}

static void __attribute__((nonnull))
print_command_line(const char * const line, void * argument)
{
    struct print_command_line_s * const pcs =
        (struct print_command_line_s *)argument;

    if (pcs->had_line_before) {
        output_putchar(pcs->cotesdex, ' ');
    }
    output_puts(pcs->cotesdex, line);
    pcs->had_line_before = true;

    return;
}

/* This function adds a comparison between file1 and file2. The cmp
 * output will be redirected to cmp_file. If the files differ diff
 * output will be written to diff_file. */
static inline void __attribute__((nonnull))
do_cmp(const cotesdex_t * const cotesdex,
       const char * const file1,
       const char * const file2,
       const char * const cmp_file,
       const char * const diff_file)
{
    oprint(
        "cmp %s %s &> %s\n"
        "case $? in\n"
        "    1)\n"
        "    msg \"%s and %s differed.\"\n"
        "    diff -u %s %s &> %s\n"
        "    echo > /dev/stderr\n"
        "    cat %s > /dev/stderr\n"
        "    echo  > /dev/stderr\n"
        "    msg \"cmp output in %s and diff output in %s\"\n"
        "    test_fail \"file difference\"\n"
        "    ;;\n"
        "    2)\n"
        "    die \"cmp failed; see cmp output in %s\"\n"
        "    ;;\n"
        "esac\n",
        file1, file2, cmp_file, file1, file2, file1, file2, diff_file,
        diff_file, cmp_file, diff_file, cmp_file);
}

static int __attribute__((nonnull))
create_output_file_checks(const cotesdex_t * const cotesdex,
                          const command_t * const command,
                          const size_t file_name_len_max,
                          const char * const here_string)
{
    int return_value = 0;

#define EXPECTED "_expected"
#define EXPECTED_LEN 9

    /* A buffer big enough to hold file_name + EXPECTED + '\0'. */
    char expected_file[file_name_len_max + EXPECTED_LEN + 1];

    /* Loop through all commands to find any output file commands and
     * compare the results. */
    for (command_t * child = command_get_child(command);
          child != NULL;
          child = command_get_sister(child))
    {
        if (command_get_type(child) == COMMAND_OUTPUT_FILE) {
            const char * const file_name = command_get_argument(child);
            char * const content = command_file_harvest_content(child);
            if (content == NULL) {
                goto error;
            }

            /* Because the length of the strings are known memcpy is
             * used becaus it is somewhat faster. strcpy() has to check
             * for the '\0' character, while memcpy() does not. */
            const size_t file_name_len = strlen(file_name);
            memcpy(expected_file, file_name, file_name_len);
            memcpy(expected_file + file_name_len, EXPECTED, EXPECTED_LEN);
            expected_file[file_name_len + EXPECTED_LEN] = '\0';

            /* The name must be suitable for a variable name! */
            make_safe(expected_file);

            /* Create the file with the expected output. */
            create_file_with_content(cotesdex, expected_file,
                                     expected_file, content,
                                     here_string);
            free(content);

            /* Create the comparison between the expected output and the
             * actual output. */
            oprint("cmp_file=" TEST_FILE_PREFIX "%s_cmp\n"
                   "diff_file=" TEST_FILE_PREFIX "%s_diff\n"
                   "expected_file=" TEST_FILE_PREFIX "%s\n"
                   "file=%s\n",
                   file_name, file_name, expected_file, file_name);
            do_cmp(cotesdex, "$expected_file", "$file", "$cmp_file",
                   "$diff_file");

            /* Remove the generated files. */
            oputs("rm $cmp_file $expected_file ");
            
            /* Also remove the checked output file if removal was
             * requested. */
            for (const command_t * c = command_get_child(child);
                  c != NULL;
                  c = command_get_sister(c))
            {
                if (command_get_type(c) == COMMAND_REMOVE) {
                    oputs("$file ");
                    break;
                }
            }
            
            oputs("|| warn \"rm failed\"\n");
        }
    }

    if (false) {
    error:
        return_value = 1;
    }

    return return_value;
}

/* Creates a test from the given test command.
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
create_test(const cotesdex_t * const cotesdex,
            const command_t * const command,
            const int test_number,
            const char * const here_string)
{
    int return_value = 0;

    struct print_command_line_s pcs;
    pcs.cotesdex = cotesdex;

    /* The maximum len of the names of the output files. */
    size_t output_file_name_len_max = 0;

    /* Assume INT_MIN is not a valid status. */
    int status = INT_MIN;

    /* Shall be free()'d. */
    char * command_line = NULL;
    char * stderr_content = NULL;
    char * stdin_content = NULL;
    char * stdout_content = NULL;

    command_t * child = command_get_child(command);
    while (child != NULL) {
        /* Variables needed for COMMAND_OUTPUT_FILE. */
        const char * output_file_name;
        size_t output_file_name_len;

        switch (command_get_type(child)) {
            case COMMAND_CMDLINE:
                command_line = command_harvest_content(child);
                child = command_unlink(child);
                break;
            case COMMAND_STATUS:
                status = atoi(command_get_argument(child));
                child = command_unlink(child);
                break;
            case COMMAND_STDERR:
                stderr_content = command_file_harvest_content(child);
                if (stderr_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDIN:
                stdin_content = command_file_harvest_content(child);
                if (stdin_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;
            case COMMAND_STDOUT:
                stdout_content = command_file_harvest_content(child);
                if (stdout_content == NULL) {
                    goto error;
                }
                child = command_unlink(child);
                break;

            /* These cases do not unlink and destroy the command. The
             * command is to be used later on. */
            case COMMAND_OUTPUT_FILE:
                output_file_name = command_get_argument(child);
                output_file_name_len = strlen(output_file_name);
                if (output_file_name_len > output_file_name_len_max) {
                    output_file_name_len_max = output_file_name_len;
                }
            default:
                child = command_get_sister(child);
                break;
        }
    }

    oprint("# Test %d\n", test_number);

    oputs("test_fail() {\n");
    oprint("    msg \"test %d failed\"\n", test_number);
    if (command_line != NULL) {
        oputs("    msg 'test command line was: ");
        pcs.had_line_before = false;
        buffer_foreach_line(command_line, &print_command_line, &pcs);
        oputs("'\n");
    } else {
        oputs("    msg \"empty command line\"\n");
    }
    oputs("    die \"test failure: ${@}\"\n"
          "}\n");

    oprint("test_tmpdir=${TMPDIR}/%d\n"
           "mkdir ${test_tmpdir} || die \"mkdir failed\"\n", test_number);

    if (stdin_content != NULL) {
        create_file_with_content(cotesdex, "stdin", "stdin",
                                 stdin_content, here_string);
    }

    OUTPUT_SET_TEST_FILE("stdout");
    if (stdout_content != NULL) {
        create_file_with_content(cotesdex, "stdout_expected",
                                 "stdout_expected", stdout_content,
                                 here_string);
    }
    OUTPUT_SET_TEST_FILE("stderr");
    if (stderr_content != NULL) {
        create_file_with_content(cotesdex, "stderr_expected",
                                 "stderr_expected", stderr_content,
                                 here_string);
    }

    if (cotesdex->memcheck) {
        oputs("if test -n \"${VALGRIND}\"\n"
              "then\n");
        oputs("    ");
        OUTPUT_SET_TEST_FILE("memcheck_log");
        oputs("    ${VALGRIND} \\\n"
              "        --tool=memcheck \\\n"
              "        --log-file=$memcheck_log \\\n"
              "        --leak-check=full \\\n"
              "        ${executable} ");
        if (command_line != NULL) {
            pcs.had_line_before = false;
            buffer_foreach_line(command_line, &print_command_line, &pcs);
            output_putchar(cotesdex, ' ');
        }
        if (stdin_content != NULL) {
            oputs("< $stdin ");
        }
        oputs("1> $stdout 2> $stderr\n"
              "    status=$?\n"
              "\n"
              "    grep -F \"ERROR SUMMARY: 0 errors from 0 contexts\" "
                       "${memcheck_log} > /dev/null\n"
              "    grep_status=$?\n"
              "    if test ${grep_status} -eq 1\n"
              "    then\n"
              "        msg \"memory errors found\"\n"
              "        msg \"see valgrind output in: $memcheck_log\"\n"
              "        test_fail \"memory errors found\"\n"
              "    elif test ${grep_status} -ne 0\n"
              "    then\n"
              "        die \"grep failed\"\n"
              "    fi\n"
              "\n"
              "    rm $memcheck_log || warn \"rm failed\"\n"
              "else\n"
              "    ${executable} ");
        if (command_line != NULL) {
            pcs.had_line_before = false;
            buffer_foreach_line(command_line, &print_command_line, &pcs);
            output_putchar(cotesdex, ' ');
        }

        if (stdin_content != NULL) {
            oputs("< $stdin ");
        }
        oputs("1> $stdout 2> $stderr\n"
              "    status=$?\n"
              "fi\n");
    } else {
        oputs("${executable} ");
        if (command_line != NULL) {
            pcs.had_line_before = false;
            buffer_foreach_line(command_line, &print_command_line, &pcs);
            output_putchar(cotesdex, ' ');
        }

        if (stdin_content != NULL) {
            oputs("< $stdin ");
        }
        oputs("1> $stdout 2> $stderr\n");
        oputs("status=$?\n");
    }

    if (status != INT_MIN) {
        /* Test for the exit status. */
        oprint("test $status -ne %d && test_fail "
               "\"expected exit status %d but got $status\"\n", status,
               status);
    }

    if (stderr_content != NULL) {
        OUTPUT_SET_TEST_FILE("stderr_cmp");
        OUTPUT_SET_TEST_FILE("stderr_diff");
        do_cmp(cotesdex, "$stderr_expected", "$stderr", "$stderr_cmp",
               "$stderr_diff");
    }

    /* Make sure we got expected output at both stderr and stdout if
     * they are specified. */
    if (stdout_content != NULL) {
        OUTPUT_SET_TEST_FILE("stdout_cmp");
        OUTPUT_SET_TEST_FILE("stdout_diff");
        do_cmp(cotesdex, "$stdout_expected", "$stdout", "$stdout_cmp",
               "$stdout_diff");
    }

    if (output_file_name_len_max > 0) {
        if (create_output_file_checks(cotesdex, command,
                                      output_file_name_len_max, here_string))
        {
            goto error;
        }
    }

    /* Remove all generated files in one call to rm. */
    oputs("rm $stderr $stdout ");
    if (stdin_content != NULL) oprint("$stdin ");
    if (stderr_content != NULL) {
        oputs("$stderr_cmp $stderr_expected ");
    }
    if (stdout_content != NULL) {
        oputs("$stdout_cmp $stdout_expected ");
    }
    oputs("|| warn \"rm failed\"\n");
    oputs("rmdir ${test_tmpdir}\n\n");

    if (false) {
    error:
        return_value = 1;
    }

    free(command_line);
    free(stderr_content);
    free(stdin_content);
    free(stdout_content);
    return return_value;
}

/* This function will set the executable variable to the specified
 * executable file name and generate code to create a temporary
 * directory based on the executable file name. */
static inline void __attribute__((nonnull))
handle_executable(const cotesdex_t * const cotesdex,
                  const char * const executable_file_name)
{
    oprint("executable=%s\n", executable_file_name);
    const char * executable_name = strrchr(executable_file_name, '/');
    assert(executable_name != NULL);
    oprint(
        "if test -z $TMPDIR\n"
        "then\n"
        "    TMPDIR=$(mktemp -d --tmpdir=$base_path .%s-XXXXXXXX)\n"
        "else\n"
        "    TMPDIR=$(mktemp -d --tmpdir=$TMPDIR .%s-XXXXXXXX)\n"
        "fi\n\n", executable_name + 1, executable_name + 1);
    return;
}

int __attribute__((nonnull))
test(cotesdex_t * const cotesdex)
{
    output_set_file_type(cotesdex, FILE_TYPE_SHELL);

    oprint("#!%s\n", BASH);

    output_begin_comment(cotesdex);
    oprint("Generated with %s\n", PACKAGE_STRING);

    char * title;
    char * description;
    char * license;
    get_meta(cotesdex, &title, &description, &license);

    /* Print title */
    if (title != NULL) {
        oprint("\n");
        oprint("Title: %s\n", title);
        free(title);
    }

    /* Print license */
    if (license != NULL) {
        oprint("\n");
        output_print_buffer(cotesdex, license);
        free(license);
    }

    /* Print description. */
    if (description != NULL) {
        oprint("\n");
        oprint("Description:\n");
        output_print_buffer(cotesdex, description);
        free(description);
    }

    output_end_comment(cotesdex);
    output_putchar(cotesdex, '\n');

    /* Test code begins here. */

    /* Set up environment specific variables. */
    oprint("base_path=%s\n", cotesdex->base_path);

    if (cotesdex->executable_file_name != NULL) {
        handle_executable(cotesdex, cotesdex->executable_file_name);
    } else {
        const char * executable;
        if (cotesdex->output_file_name != NULL) {
            executable = cotesdex->output_file_name;
        } else if (cotesdex->input_file_name != NULL) {
            executable = cotesdex->input_file_name;
        } else {
            print_error(_("could not guess the name of the executable file to "
                          "test"));
            print_error(_("please supply --executable=EXECUTABLE on the "
                          "command line"));
            return 1;
        }

        const size_t len = strlen(executable);
        char buffer[len + 1];
        memcpy(buffer, executable, len + 1);

        /* Just remove the suffix if there is one. */
        char * const c = strrchr(buffer, '.');
        if (c != NULL) {
            *c = '\0';
        }
        handle_executable(cotesdex, buffer);
    }

    create_helpers(cotesdex);

    if (cotesdex->memcheck) {
        oprint("if test -z \"${VALGRIND}\"\n"
               "then\n"
               "    VALGRIND=\"%s\"\n"
               "fi\n"
               "\n"
               "if ! ${VALGRIND} --version &> /dev/null\n"
               "then\n"
               "    warn \"the valgrind program \\\"${VALGRIND}\\\" does not "
                          "work\"\n"
               "    VALGRIND=\n"
               "fi\n"
               "\n",
               (cotesdex->valgrind != NULL) ? cotesdex->valgrind : VALGRIND);
    }

    /* Change directory to the base path so that all input files can
     * be specified relative to the base path in the input file. */
    oprint("cd $base_path || die \"cd failed\"\n\n");

    const char * here_string;
    if (cotesdex->here_string == NULL) {
        here_string = "__COTESDEX_HERE_STRING__";
    } else {
        here_string = cotesdex->here_string;
    }

    int test_number = 1;
    const command_t * const root = cotesdex->commands;
    const command_t * command = command_get_child(root);
    while (command != NULL) {
        if (command_get_type(command) == COMMAND_TEST) {
            if (create_test(cotesdex, command, test_number++,
                             here_string))
            {
                return 1;
            }
        }
        command = command_get_sister(command);
    }

    oprint("rmdir $TMPDIR &> /dev/null\n"
           "exit 0\n");

    if (cotesdex->output_file_name != NULL) {
        struct stat buf;
        if (stat(cotesdex->output_file_name, &buf)) {
            print_error_errno("could not stat %s", cotesdex->output_file_name);
            return 1;
        }
        if (chmod(cotesdex->output_file_name, buf.st_mode | S_IXUSR)) {
            print_error_errno("could not chmod %s", cotesdex->output_file_name);
            return 1;
        }
    }

    return 0;
}
