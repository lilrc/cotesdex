#!/bin/sh
#
#  This file is part of cotesdex.
#
#  Copyright (C) 2015 Karl Lindén <lilrc@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

run() {
    echo "running ${@}"
    ${@}
    local status=$?
    if test ${status} -ne 0
    then
        echo "${@} failed"
        exit ${status}
    fi
}

check_and_run() {
    tool=${1}
    shift
    args=${@}

    echo -n "checking for ${tool}... "
    if command -v ${tool} > /dev/null 2>&1
    then
        echo "found"
    else
        echo "not found"
        echo "You do not have ${tool} correctly installed. You will not"
        echo "be able to build this package without it."
        exit 1
    fi

    run ${tool} ${args}
    echo
}

run git submodule update --init .gnulib

check_and_run autopoint --force

gnulib_modules="
    canonicalize-lgpl
    getcwd
    gettext
    propername
    stdbool
    stddef
    stdlib
    strerror
"
.gnulib/gnulib-tool \
    --conditional-dependencies \
    --source-base=lib \
    --m4-base=lib/m4 \
    --no-vc-files \
    --import ${gnulib_modules}

run cp .gnulib/build-aux/po/Makefile.in.in po/Makefile.in.in

check_and_run aclocal -I m4 -I lib/m4
check_and_run autoheader
check_and_run automake --add-missing --copy
check_and_run autoconf

echo "You can now run:"
echo "    ./configure"
echo "    make"
echo "    make install"

exit 0
