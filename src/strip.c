/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* strip.c - routines needed to strip an input file */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include "file-type.h"
#include "main.h"
#include "output.h"
#include "strip.h"

int
strip(cotesdex_t * const cotesdex)
{
    output_set_file_type(cotesdex, cotesdex->input_file_type);

    output_begin_comment(cotesdex);
    output_print(cotesdex, "Generated with %s\n", PACKAGE_STRING);

    char * title;
    char * description;
    char * license;
    get_meta(cotesdex, &title, &description, &license);

    /* Print title */
    if (title != NULL) {
        output_print(cotesdex, "\n");
        output_print(cotesdex, "Title: %s\n", title);
        free(title);
    }

    /* Print license. */
    if (license != NULL) {
        output_print(cotesdex, "\n");
        output_print_buffer(cotesdex, license);
        free(license);
    }

    /* Print description. */
    if (description != NULL) {
        output_print(cotesdex, "\n");
        output_print(cotesdex, "Description:\n");
        output_print_buffer(cotesdex, description);
        free(description);
    }

    output_end_comment(cotesdex);

    output_print(cotesdex, "\n%s\n", cotesdex->code);

    return 0;
}
