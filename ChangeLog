== Cotesdex ChangeLog ==

0.3.0  2015-02-23  A slightly bigger deal

- Slot 3 of GOP is now required, which corresponds to the latest version
  available (3.0.0).
- Gnulib is used to ease porting.
- Valgrind support is now mandatory, since there was no reason to have it
  conditional.
- Valgrind is no longer a build time dependency (it is detected at runtime).
- Make the entire program translatable.
- Remove compilation warnings with -Wsign-compare, -Wconversion and
  -Wsign-conversion.
- The cotesdex-valgrind script should be safer than before.
- Small code fixes such as invalid use of fputs.
- Optimizations
  - Remove some unnecessary function calls.
  - Replace some function calls.
  - Use unlocked IO if possible.
- Improve error printing.
- Coding style has changed.
  - Spaces are used instead of tabs.
  - Line width is now 80 characters instead of 72.
  - Less spaces in parentheses.
- Other small various fixes.

0.2.2  2014-04-12  No big deal

- Slot 1 of GOP is now required, which corresponds to the latest
  version (1.0.0).
- Some minor behavioural changes in the option parsing code. 
- Small and almost insignificant changes here and there.

0.2.1  2013-12-31  Very minor bugfix release

- Version 0.2.0 came with a bug that made parallel tests fail if they
  were all fed with the same value of $TMPDIR. This bug is fixed.
- The build system no longer checks for ranlib on configuration.

0.2.0  2013-12-31  Major release with many new features and changes.

- Implement functionality to parse raw files, that are files that are
  not written in any particular programming language.
- Add the --file-type option so that the file types (currently only raw
  and c) can be manually controlled if necessary.
- The tests now respect $TMPDIR better.
- Commands and tags are now lower case (to aid writing) and indentation
  sensitive. Files working with cotesdex 0.1.x are not compatible with
  the new command parsing mechanisms.
- Some commands have been added, removed, renamed or changed.
- Introduce a (currently small) test suite.
- Introduce the base path which is the path where cotesdex will look
  for files. The path can be manually set with the --base-path option.
- BASH is now strictly needed.
- Diff output is now shown if two files differ in the tests.
- The here string cotesdex uses in the generated tests can now be
  manually set with the --here-string option.
- Cotesdex can now be built and installed with valgrind support. This is
  controlled with the new --with-valgrind option to configure. This will
  add two options to cotesdex --memcheck to run test executables in
  valgrind's memcheck tool and --valgrind to specify a non-default
  valgrind to use.
- Some error reporting has been improved.
- Huge internal code changes.

0.1.0  2013-11-08  Almost complete rewrite of the source.

- _Big_ rewrite
- The code is cleaner and more understandable.
- All debug specific code has been removed because it is no longer
  needed due to the simpler code.
- Some styling of output files has changed.
- The tab size is no longer hardcoded and faulty.
- Error printing has been improved.

0.0.1  2013-08-21  First versioned release

- Initial release.
