/* 
 * This file is part of cotesdex.
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* read_file.h - reads the content of a file and saves it in a buffer */

#ifndef READ_FILE_H
# define READ_FILE_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <stddef.h>

/* Reads all content of file into a buffer and returns it. The read file
 * end (that is the newline at the end of the file) will be replaced
 * with the NUL character.
 * 
 * The result should be free()'d after use.
 * 
 * Returns the allocated buffer on success and NULL on failure. */
char * read_file(const char * const file_name, size_t * const buffer_len)
    __attribute__((nonnull));

#endif /* !READ_FILE_H */
