/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* parser.c - framework to parse input */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "command.h"
#include "io.h"
#include "main.h"
#include "nls.h"
#include "parser.h"
#include "print-error.h"
#include "read-file.h"
#include "xalloc.h"

#define print_note_at_pointer(pointer, ...) \
    print_at_pointer(pointer, _("note"), __VA_ARGS__)
#define print_error_at_pointer(pointer, ...) \
    print_at_pointer(pointer, _("error"), __VA_ARGS__)

/* The size of each chunk (in chars) that should be read from standard
 * input. */
#define STDIN_READ_CHUNK_SIZE 256

#define COMMAND_TAG "@cotesdex"

#define MULTILINE_COMMENT_BEGIN "/* "
#define MULTILINE_COMMENT_BEGIN_LEN 3
#define MULTILINE_COMMENT_END " */"
#define MULTILINE_COMMENT_END_LEN 3
#define ONELINE_COMMENT_BEGIN "// "

typedef struct pointer_s pointer_t;
typedef struct parser_s parser_t;

struct parser_s {
    /* This is the buffer where all content of the currently parsed file
     * is saved. Eventually, the code is also saved in this buffer. As
     * the program traverses the file the content of the buffer changes
     * to the code of the file.
     * 
     * The size of the buffer is buffer_len + 1. The buffer is NUL-
     * terminated, so buffer[buffer_len] == '\0'. */
    char * buffer;
    size_t buffer_len;
    size_t code_len;

    const char * file_name;

    /* The current command. */
    command_t * command;

    /* The number of times the hide command occurs in the stack. When it
     * is more than 1 no code will be dumped. */
    unsigned int hide_count;

    /* Tabs will stop at multiples of the tabsize. */
    unsigned short tabsize;
};

#define parser_in_command(parser) \
    (command_get_type((parser)->command) != COMMAND_ROOT)

/* This is a higher level pointer to the a place in the buffer. */
struct pointer_s {
    char * ptr;
    unsigned int line;
    unsigned short column;

    parser_t * parser;
};

/* Reads stdin until EOF is reached and saves the data into a buffer.
 * The buffer should be free()'d when used.
 * Returns the newly allocated buffer on success and NULL on failure. */
static char *
read_stdin(size_t * const buffer_len)
{
    /* The buffer to fill in. */
    char * buffer = NULL;
    /* The allocated size of the buffer in chars. */
    size_t buffer_size = 0;

    *buffer_len = 0;

    while (!feof(stdin)) {
        /* Extend the buffer. */
        buffer_size += STDIN_READ_CHUNK_SIZE;
        buffer = xrealloc(buffer, buffer_size);
        *buffer_len += fread(buffer + *buffer_len, 1, STDIN_READ_CHUNK_SIZE,
                             stdin);

        /* Make sure no error has occured. */
        if (ferror(stdin)) {
            print_error(_("failed to read standard input"));
            free(buffer);
            return NULL;
        }
    }

    buffer = xrealloc(buffer, *buffer_len + 1);

    buffer[*buffer_len] = '\0';

    return buffer;
}

/* Initializes the pointer object that is pointer to by pointer. The
 * pointer object does not initially point to something and should not
 * be used without making sure that the pointer object points to
 * something. */
static inline void __attribute__((nonnull))
pointer_init(pointer_t * const pointer, parser_t * const parser)
{
    pointer->parser = parser;
    return;
}

/* Makes pointer1 pointer1 point to the same as pointer2. */
static inline void  __attribute__((nonnull))
pointer_point(pointer_t * const pointer1, const pointer_t pointer2)
{
    pointer1->ptr = pointer2.ptr;
    pointer1->line = pointer2.line;
    pointer1->column = pointer2.column;
    return;
}

/* Initializes pointer1 and makes it point to the same place as
 * pointer2. */
static inline void __attribute__((nonnull))
pointer_duplicate(pointer_t * const pointer1, const pointer_t pointer2)
{
    pointer_init(pointer1, pointer2.parser);
    pointer_point(pointer1, pointer2);
    return;
}

/* Makes the pointer object point on the beginning of the buffer. */
static inline void __attribute__((nonnull))
pointer_point_beginning(pointer_t * const pointer)
{
    pointer->ptr = pointer->parser->buffer;
    pointer->line = 1;
    pointer->column = 0;
    return;
}

/* Step the pointer.
 * Returns 1 if the pointer object was stepped and 0 if the current
 * character is the end and the pointer object was not stepped. */
static int __attribute__((hot, nonnull))
pointer_step(pointer_t * const pointer)
{
    switch (*pointer->ptr) {
        case '\n':
            pointer->line++;
            pointer->column = 0;
            break;
        case '\t':
            /* Indent the current column to a multiple of the tabisize. */
            pointer->column =
                (unsigned short)(pointer->column + pointer->parser->tabsize -
                    pointer->column % pointer->parser->tabsize); 
            break;
        case '\0':
            return 0;
        default:
            pointer->column++;
            break;
    }

    pointer->ptr++;
    return 1;
}

/* Steps the pointer strlen(string) steps. The actual pointer values are
 * not checked, so they have to be checked before. */
static inline void __attribute__((nonnull))
pointer_skip_string(pointer_t * const pointer,
                    const char * const restrict string)
{
    for (size_t i = strlen(string); i > 0; --i) {
        pointer_step(pointer);
    }
    return;
}

/* Print a message with extra information from the pointer. */
static void __attribute__((nonnull, format (printf, 3, 4)))
print_at_pointer(const pointer_t pointer,
                 const char * const what,
                 const char * fmt,
                 ...)
{
    const parser_t * const parser = pointer.parser;

    /* Print the location of the pointer. */
    fprintf(stderr, "%s: ", PACKAGE);
    if (parser->file_name != NULL) {
        fprintf(stderr, "%s:", parser->file_name);
    }
    fprintf(stderr, "%u:%u: %s: ", pointer.line, pointer.column, what);

    /* Print the message. */
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fputc('\n', stderr);

    /* Print the context of the error. */
    const char * const str = PACKAGE ": ";
    const unsigned short col_begin = (unsigned short)strlen(str);
    fputs(str, stderr);

    /* Get a pointer to the first character on the line. */
    const char * c = pointer.ptr;

    /* The do-while loop here is needed if the character at the pointer
     * is a line break. */
    do {
        --c;
    } while (*c != '\n' && c != pointer.parser->buffer);
    if (c != pointer.parser->buffer) {
        ++c;
    }

    /* Save the pointer to the beginning of the line. Used to make sure
     * tab stops are spaced correctly. */
    const char * const line_begin = c;

    /* Print the line. */
    for (; *c != '\n' && *c != '\0'; ++c) {
        if (*c != '\t') {
            fputc(*c, stderr);
        } else {
            /* Print spaces until the next tab stop. */
            for (unsigned short i =
                     (unsigned short)((c - line_begin) % parser->tabsize);
                  i < parser->tabsize;
                  ++i)
            {
                fputc(' ', stderr);
            }
        }
    }
    fputc('\n', stderr);

    /* Print a caret (^) to represent the pointer */
    unsigned short col = (unsigned short)(col_begin + pointer.column);
    while (col--) {
        fputc(' ', stderr);
    }
    fprintf(stderr, "^\n"); 

    return;
}

/* Initializes a parser object.
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull (1,2)))
parser_init(parser_t * const parser,
            cotesdex_t * const cotesdex,
            const unsigned short tabsize)
{
    parser->tabsize = tabsize;

    parser->file_name = cotesdex->input_file_name;
    if (parser->file_name != NULL) {
        parser->buffer = read_file(parser->file_name,
                                   &parser->buffer_len);
    } else {
        parser->buffer = read_stdin(&parser->buffer_len);
    }

    if (parser->buffer == NULL) {
        return 1;
    }
    parser->code_len = 0;

    /* Set the current command to the command root of the cotesdex
     * structure. */
    parser->command = cotesdex->commands;

    parser->hide_count = 0;

    return 0;
}

/* Steps up the current command to its parent after finishing it. */
static void __attribute__((nonnull))
parser_step_to_parent_command(parser_t * const parser)
{
    command_t * const parent = command_get_parent(parser->command);
    if (parent != NULL) {
        /* Save the current command for finishing and replace it with
         * the parent. */
        command_t * const command = parser->command;
        parser->command = parent;

        /* Finish the command. */
        if (command->content != NULL) {
            command->content = xrealloc(command->content,
                                        command->content_len + 1);
            command->content[command->content_len] = '\0';
        }
        
    }
}

/* Find out if s2 is at s1. */
static bool __attribute__((hot, nonnull, pure))
string_at(const char * restrict s1, const char * restrict s2)
{
    while (*s2 != '\0') {
        /* This is safe because if the NUL-character is reached this
         * comparison will always evaluate to true and return. This is
         * because *s2 is not '\0'.  */
        if (*s1 != *s2) {
            return false;
        }
        s1++;
        s2++;
    }
    return true;
}

/* Returns true if the string string is at the pointer pointer or false
 * if it is not or string is NULL. */
static inline bool __attribute__((hot, pure))
string_at_pointer(const pointer_t pointer,
                  const char * const restrict string)
{
    if (string != NULL) {
        return string_at(pointer.ptr, string);
    } else {
        return false;
    }
}

static inline bool __attribute__((pure))
multiline_comment_begin_at_pointer(const pointer_t pointer)
{
    return string_at(pointer.ptr, MULTILINE_COMMENT_BEGIN);
}

static inline bool __attribute__((pure))
multiline_comment_end_at_pointer(const pointer_t pointer)
{
    return string_at(pointer.ptr, MULTILINE_COMMENT_END);
}

static inline bool __attribute__((pure))
oneline_comment_begin_at_pointer(const pointer_t pointer)
{
    return string_at(pointer.ptr, ONELINE_COMMENT_BEGIN);
}

static inline bool __attribute__((pure))
tag_at_pointer(const pointer_t pointer)
{
    return string_at(pointer.ptr, COMMAND_TAG);
}

/* Dumps the range between pointer1 and pointer2 to the buffer at
 * buffer_len and updates buffer_len. The buffer will not be
 * reallocated, so the buffer should be large enough to hold the
 * additional characters. */ 
static inline void __attribute__((nonnull))
dump_range(char * const buffer,
           size_t * const buffer_len,
           const pointer_t pointer1,
           const pointer_t pointer2)
{
    const long int size = pointer2.ptr - pointer1.ptr;
    if (size > 0) {
        memcpy(buffer + *buffer_len, pointer1.ptr, (size_t)size);
        *buffer_len += (size_t)size;
    }
    return;
}

/* Dumps the data between pointer1 including and pointer2 excluding to
 * the code. After the call to this function the pointer1 should not be
 * used again because the content from pointer1 (inclusive) up to
 * pointer2 (exclusive) is not guaranteed to be unchanged. */
static inline void __attribute__((nonnull))
dump_range_to_code(parser_t * const parser,
                   const pointer_t pointer1,
                   const pointer_t pointer2)
{
    if (!parser->hide_count) {
        const long int size = pointer2.ptr - pointer1.ptr;
        if (size > 0) {
            memmove(parser->buffer + parser->code_len, pointer1.ptr,
                    (size_t)size);
            parser->code_len += (size_t)size;
        }
    }
    return;
}

/* Dumps the string to the code member of the parser updating code_len.
 * Note that some pointers might turn invalid if the string is enough
 * big. That is if
 * parser->buffer + parser->code_len + strlen(string) > pointer->ptr. */
static inline void __attribute__((nonnull))
dump_string_to_code(parser_t * const parser,
                    const char * const restrict string,
                    const size_t len)
{
    assert(len == strlen(string));
    memcpy(parser->buffer + parser->code_len, string, len);
    parser->code_len += len;
    return;
}

/* A small helper function to parse_command() that will step the pointer
 * to the end of the line or the comment end sequence. The function will
 * fail if it encounters any non-space.
 * Returns 0 on success and 1 on failure. */
static inline int __attribute__((nonnull))
step_to_end(pointer_t * const pointer,
            const char * const restrict comment_end)
{
    do {
        if (*pointer->ptr == '\n' ||
            string_at_pointer(*pointer, comment_end) ||
            *pointer->ptr == '\0')
        {
            break;
        } else if (!isspace(*pointer->ptr)) {
            print_error_at_pointer(*pointer, 
                                   _("unexpected non-space character (%c) "
                                     "after command"),
                                   *pointer->ptr);
            return 1;
        }
    } while (pointer_step(pointer));
    return 0;
}

/* Parses a command at the pointer. The function will step the pointer.
 * When the function returns successfully pointer will point to either
 * the linefeed character at the end of the line or a comment end
 * sequence.
 * 
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull (1)))
parse_command(pointer_t * const pointer,
              const char * const restrict comment_end)
{
    parser_t * const parser = pointer->parser;

    /* Save the current column so that it can be filled in into the
     * command structure later on. */
    const unsigned short column = pointer->column;

    /* Step the current command so that it is the parent of the command
     * to be added. Step it at most to the root element, that is as long
     * as the parser is in a command. */
    while (parser->command->column >= column &&
            parser_in_command(parser))
    {
        parser_step_to_parent_command(parser);
    }

    /* Get the synopsis here already so that error message is pointing
     * to the tag. */
    const command_synopsis_t * synopsis =
        parser->command->synopsis->children;
    if (synopsis == NULL) {
        print_error_at_pointer(*pointer,
                               _("child command at command that cannot have "
                                 "child commands"));
        return 1;
    }

    /* Skip the tag. */
    pointer_skip_string(pointer, COMMAND_TAG);

    /* Skip whitespaces. */
    while (true) {
        const char c = *pointer->ptr;
        if (c == '\n' ||
             string_at_pointer(*pointer, comment_end) ||
             c == '\0')
        {
            /* This is a nul command and will only step the parser back
             * to the closest parent (and finish up all commands). That
             * is already done so just quit. */
            return 0;
        } else if (!isspace(c)) {
            break;
        }
        pointer_step(pointer);
    }

    /* The next string should be the command name. */
    while (synopsis->type != COMMAND_INVALID) {
        if (string_at(pointer->ptr, synopsis->name)) {
            pointer_skip_string(pointer, synopsis->name);
            break;
        }
        ++synopsis;
    }

    switch (synopsis->type) {
        case COMMAND_INVALID:
            /* There should have been a valid command name. */
            print_error_at_pointer(*pointer, _("unknown command"));
            return 1;
        case COMMAND_HIDE:
            /* Don't create a new command; it is waste of space. Just
             * increment hide_count. */
            parser->hide_count++;
            return step_to_end(pointer, comment_end);
        case COMMAND_UNHIDE:
            /* Don't create a new command; it is waste of space. Just
             * decrement hide_count. */
            if (parser->hide_count > 0) {
                parser->hide_count--;
            }
            return step_to_end(pointer, comment_end);
        default:
            break;
    }

    command_t * const command = command_new(synopsis);

    if (synopsis->needs_argument) {
        /* Skip leading spaces and make sure there is an argument. */
        while (isspace(*pointer->ptr)) {
            if (*pointer->ptr == '\n' ||
                 string_at_pointer(*pointer, comment_end))
            {
                print_error_at_pointer(*pointer, _("argument expected"));
                command_destroy(command);
                return 1;
            }
            pointer_step(pointer);
        }

        /* This is done so that trailing whitespaces between the
         * argument and the linefeed or the comment end sequence are
         * ignored. */
        pointer_t middle;
        pointer_t end;
        pointer_duplicate(&middle, *pointer);
        pointer_duplicate(&end, *pointer);

        while (pointer_step(&end) &&
                *end.ptr != '\n' &&
                !string_at_pointer(end, comment_end))
        {
            if (!isspace(*end.ptr)) {
                pointer_point(&middle, end);
            }
        }

        /* middle now points to the last character in the argument and
         * end points to the linefeed or the comment end sequence. */
        /* Step middle so that the last character is included. */
        pointer_step(&middle);

        /* Allocate the argument memeber of command. */
        const size_t len = (size_t)(middle.ptr - pointer->ptr);
        char * const argument = xmalloc(len + 1);

        memcpy(argument, pointer->ptr, len);
        argument[len] = '\0';
        command->argument = argument;

        pointer_point(pointer, end);
    } else {
        if (step_to_end(pointer, comment_end)) {
            command_destroy(command);
            return 1;
        }
    }

    /* Fill in the column where the tag was found. */
    command->column = column;

    command_add_child(parser->command, command);
    if (synopsis->can_have_content) {
        /* Allocate the content buffer. Allocate the maximum size and
         * resize the buffer when the content is written. */
        const size_t traversed = (size_t)(pointer->ptr - parser->buffer);
        const size_t size = parser->buffer_len - traversed;
        command->content = xmalloc(size);
        command->content_len = 0;

        /* This is now the current command. */
        parser->command = command;
    } else if (synopsis->children != NULL) {
        parser->command = command;
    }

    return 0;
}

/* Parses the comment that starts at pointer1 and pointer2. The two
 * pointer needs to point to the same place.
 * 
 * The function will step both pointers so the pointer objects will
 * change. After this call the pointer objects will both point to the
 * character right after the comment end.
 * 
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
parse_multiline_comment(pointer_t * const restrict pointer1,
                        pointer_t * const restrict pointer2)
{
    parser_t * const parser = pointer1->parser;

    /* Set to true if there is a tag in the comment. */
    bool has_tag = false;

    /* Don't catch any content or tags before this column. */
    const unsigned short comment_column =
        (unsigned short)(pointer1->column + MULTILINE_COMMENT_BEGIN_LEN);

    /* First traverse the comment to see if it has any tag. */
    while (true) {
        pointer_step(pointer2);
        if (*pointer2->ptr == '\0') {
            print_error_at_pointer(*pointer1, _("unterminated comment"));
            return 1;
        }

        /* This check has to go first. We don't want to leave the
         * comment. */
        if (multiline_comment_end_at_pointer(*pointer2)) {
            /* Step over the comment end. */
            pointer_skip_string(pointer2, MULTILINE_COMMENT_END);
            break;
        }

        const char c = *pointer2->ptr;

        if (pointer2->column < comment_column) {
            /* Require that nothing but spaces (or tabs) is before the
             * comment column. The only exception to this is the
             * asterisk because it is used like in this comment. */
            if (c != '*' && !isspace(c)) {
                print_error_at_pointer(*pointer2,
                    _("non-space character (%c) before comment "
                    "indentation level (%u)"), c, comment_column);
                print_note_at_pointer(*pointer1, _("comment began here:"));
                return 1;
            }
            continue;
        }

        /* Catch a tag. */
        if (!has_tag && tag_at_pointer(*pointer2)) {
            has_tag = true;
        }
    }

    if (has_tag || parser_in_command(parser)) {
        /* This comment has to be parsed very carefully.
         * The initial situation is now that pointer1 points to the
         * comment begin sequence and pointer2 points to the character
         * right after the comment end. */
        pointer_t line_begin;
        pointer_t comment_begin;
        pointer_init(&line_begin, parser);
        pointer_init(&comment_begin, parser);

        /* Just rename pointer1 to something more intuitive. */
        pointer_t * const current = pointer1;

        bool has_dumped_code = false;
        do {
            if (current->column == 0) {
                pointer_point(&line_begin, *current);
            }

            /* Don't catch anything before the indentation of the
             * comment. The commands indentation is not relevant here
             * because there might be other commands at lower
             * indentation level. */
            if (current->column < comment_column)
            {
                continue;
            } else if (current->column == comment_column) {
                pointer_point(&comment_begin, *current);
            }

            if (*current->ptr == '\n' ||
                 multiline_comment_end_at_pointer(*current))
            {
                /* Flush a range to the correct place. */
                if (parser_in_command(parser) &&
                     parser->command->content != NULL)
                {
                    command_t * const command = parser->command;
                    /* Step comment_begin to the command indentation and
                     * also make sure it is not stepped beyond the
                     * current pointer. */
                    while (comment_begin.column < command->column &&
                            comment_begin.ptr != current->ptr)
                    {
                        pointer_step(&comment_begin);
                    }

                    if (command->content_len > 0) {
                        /* Dump a line feed. */
                        command->content[command->content_len++] = '\n';
                    }
                    /* Dump comment_begin -> current to the command
                     * content. */
                    dump_range(command->content, &command->content_len,
                               comment_begin, *current);
                } else if (!parser->hide_count) {
                    if (has_dumped_code) {
                        /* Dump a line feed then dump line_begin ->
                         * current to the code. */
                        parser->buffer[parser->code_len++] = '\n';
                        dump_range_to_code(parser, line_begin,
                                           *current);
                    } else {
                        /* Dump the comment begin sequence and then
                         * dump comment_begin -> current. */
                        const size_t len = MULTILINE_COMMENT_BEGIN_LEN;
                        dump_string_to_code(parser,
                                            MULTILINE_COMMENT_BEGIN,
                                            len);
                        dump_range_to_code(parser, comment_begin,
                                           *current);
                        has_dumped_code = true;
                    }
                }
            } else if (tag_at_pointer(*current)) {
                if (parse_command(current, MULTILINE_COMMENT_END)) {
                    return 1;
                }
            }
        } while (pointer_step(current) &&
                  current->ptr != pointer2->ptr);

        if (has_dumped_code) {
            /* Make sure the comment end sequence is dumped to the
             * code. */
            dump_string_to_code(parser, MULTILINE_COMMENT_END,
                                MULTILINE_COMMENT_END_LEN);
        }
    } else {
        /* The comment should just be dumped. */
        dump_range_to_code(parser, *pointer1, *pointer2);
        pointer_point(pointer1, *pointer2);
    }

    return 0;
}

/* Parses the comment that starts at pointer1 and pointer2. The two
 * pointer needs to point to the same place.
 * 
 * The function will step both pointers so the pointer objects will
 * change. After this call the pointer objects will both point to the
 * beginning of the next line.
 * 
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
parse_oneline_comment(pointer_t * const restrict pointer1,
                      pointer_t * const restrict pointer2)
{
    parser_t * const parser = pointer1->parser;

    /* Set to true if there is a tag in the comment. */
    bool has_tag = false;

    /* Traverse the comment to see if there is a tag. */
    while (pointer_step(pointer2) && pointer2->column != 0) {
        if (!has_tag && tag_at_pointer(*pointer2)) {
            has_tag = true;
            pointer_point(pointer1, *pointer2);
        }
    }

    if (has_tag) {
        /* pointer1 points to the tag. */
        if (parse_command(pointer1, NULL)) {
            return 1;
        }
    } else {
        if (parser_in_command(parser)) {
            command_t * const command = parser->command;
            if (command->content_len != 0) {
                command->content[command->content_len++] = '\n';
            }

            /* Make pointer1 point on the beginning of the comment. */
            pointer_skip_string(pointer1, ONELINE_COMMENT_BEGIN);

            dump_range(command->content, &command->content_len,
                       *pointer1, *pointer2);
        } else {
            /* Just dump. */
            dump_range_to_code(parser, *pointer1, *pointer2);
        }
    }

    pointer_point(pointer1, *pointer2);
    return 0;
}

/* Parse the input as C source code.
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
parse_c(parser_t * const parser)
{
    /* Get a pair of pointers and point them to the beginning of the
     * buffer. */
    pointer_t pointer1;
    pointer_t pointer2;
    pointer_init(&pointer1, parser);
    pointer_point_beginning(&pointer1);
    pointer_duplicate(&pointer2, pointer1);

    do {
    again:
        if (multiline_comment_begin_at_pointer(pointer2)) {
            dump_range_to_code(parser, pointer1, pointer2);
            pointer_point(&pointer1, pointer2);
            if (parse_multiline_comment(&pointer1, &pointer2)) {
                return 1;
            } else {
                goto again;
            }
        }
        if (oneline_comment_begin_at_pointer(pointer2)) {
            dump_range_to_code(parser, pointer1, pointer2);
            pointer_point(&pointer1, pointer2);
            if (parse_oneline_comment(&pointer1, &pointer2)) {
                return 1;
            } else {
                goto again;
            }
        }
    } while (pointer_step(&pointer2));

    /* Dump the last code. */
    dump_range_to_code(parser, pointer1, pointer2);

    return 0;
}

/* Parses the input as raw input, i.e. no programming language.
 * Returns 0 on success and 1 on failure. */
static int __attribute__((nonnull))
parse_raw(parser_t * const parser)
{
    pointer_t pointer1;
    pointer_t pointer2;
    pointer_t line_begin;
    pointer_init(&pointer1, parser);
    pointer_point_beginning(&pointer1);
    pointer_duplicate(&pointer2, pointer1);
    pointer_duplicate(&line_begin, pointer1);

    do {
        /* This clause is jumped to whenever the loop body should be run
         * again after stepping pointer2 and pointing pointer1 to
         * pointer2. */
        if (false) {
        again:
            /* pointer2 points to the linefeed character at the end
             * of the line. Step it so that the linefeed is not
             * included in the next dump. */
            pointer_step(&pointer2);
            pointer_point(&pointer1, pointer2);
        }

        if (!parser_in_command(parser)) {
            if (tag_at_pointer(pointer2)) {
                dump_range_to_code(parser, pointer1, pointer2);
                if (parse_command(&pointer2, NULL)) {
                    return 1;
                }
                goto again;
            }
        } else {
            command_t * const command = parser->command;

            if (pointer2.column == 0) {
                pointer_point(&line_begin, pointer2);
            }

            if (pointer2.column == command->column) {
                pointer_point(&pointer1, pointer2);
            }

            if (*pointer2.ptr == '\n') {
                if (command->content_len > 0) {
                    /* A linefeed character has to be appended to the
                     * content. */
                    command->content[command->content_len++] = '\n';

                    dump_range(command->content, &command->content_len,
                               pointer1, pointer2);
                    goto again;
                } else {
                    if (command->content != NULL) {
                        dump_range(command->content,
                                   &command->content_len, pointer1,
                                   pointer2);
                    } else {
                        dump_range_to_code(parser, line_begin,
                                           pointer2);
                    }
                    goto again;
                }
            } else if (tag_at_pointer(pointer2)) {
                /* Dump all content before the tag. */
                if (command->content != NULL) {
                    dump_range(command->content, &command->content_len,
                               pointer1, pointer2);
                } else {
                    dump_range_to_code(parser, line_begin, pointer2);
                }

                /* Parse the command. */
                if (parse_command(&pointer2, NULL)) {
                    return 1;
                }
                goto again;
            }
        }
    } while (pointer_step(&pointer2));

    /* Dump the last code. */
    dump_range_to_code(parser, pointer1, pointer2);

    return 0;
}

/* 0 on success and 1 on failure. */
int __attribute__((nonnull (1)))
parser_process(cotesdex_t * const cotesdex, const unsigned short tabsize)
{
    int return_value = 0;

    parser_t parser;
    if (parser_init(&parser, cotesdex, tabsize)) {
        return 1;
    }

    switch (cotesdex->input_file_type) {
        case FILE_TYPE_C:
            if (parse_c(&parser)) {
                goto error;
            }
            break;
        case FILE_TYPE_RAW:
            if (parse_raw(&parser)) {
                goto error;
            }
            break;
        default:
            /* This should never happen! */
            print_error("internal fatal error: unkown file type");
            goto error;
    }

    /* Finish all unfinished commands. */
    while (parser_in_command(&parser)) {
        parser_step_to_parent_command(&parser);
    }

    parser.buffer = xrealloc(parser.buffer, parser.code_len + 1);
    parser.buffer[parser.code_len] = '\0';

    /* Fill in the supplied data. */
    cotesdex->code = parser.buffer;
    cotesdex->code_len = parser.code_len;

    if (false) {
    error:
        return_value = 1;
        free(parser.buffer);
    }

    return return_value;
}
