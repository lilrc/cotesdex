/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* file_type.h - routines and data types for handling file types. */

#ifndef FILE_TYPE_H
# define FILE_TYPE_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* An enumeration of known file types both input and output. */
enum file_type_e {
    FILE_TYPE_ASCIIDOC,
    FILE_TYPE_C,
    FILE_TYPE_RAW,
    FILE_TYPE_SHELL
};
typedef enum file_type_e file_type_t;

/* The markup structure defining how comments look in the different file
 * types. */
struct markup_s {
    /* Multiline comment. */
    const char * comment_begin;
    const char * comment_end;
    const char * comment_line;

    /* One line comment. */
    const char * comment_oneline;
};
typedef struct markup_s markup_t;

/* This function attempts to autodetect the file type of the specified
 * input file.
 * Returns the autodetected file type. */
file_type_t file_type_autodetect(const char * const file_name)
    __attribute__((pure));

/* Gets the markup for a specified file type.
 * Returns a pointer to the requested markup or NULL if the requested
 * markup was not found. */
const markup_t * file_type_get_markup(const file_type_t file_type)
    __attribute__((const));

#endif /* !FILE_TYPE_H */
