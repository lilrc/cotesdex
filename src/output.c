/* 
 * This file is part of cotesdex
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* output.c - common functions for handling output of data */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "buffer-foreach-line.h"
#include "io.h"
#include "main.h"
#include "nls.h"
#include "output.h"
#include "print-error.h"

/* Returns a member from the markup if it is non-NULL or the oneline
 * comment sequence if the member was NULL. */
#define get_markup_member(markup, member) \
    (((markup)->member != NULL) ? \
        ((markup)->member):(((markup)->comment_oneline)))
#define comment_begin(markup) get_markup_member((markup), comment_begin)
#define comment_end(markup)   get_markup_member((markup), comment_end)
#define comment_line(markup)  get_markup_member((markup), comment_line)

int __attribute__((nonnull))
output_open_file(cotesdex_t * const cotesdex,
                 const char * const file_name)
{
    cotesdex->output_file = fopen(file_name, "w");
    if (cotesdex->output_file == NULL) {
        print_error_errno(_("could not open %s for writing"), file_name);
        return 1;
    }

    cotesdex->output_file_name = realpath(file_name, NULL);
    if (cotesdex->output_file_name == NULL) {
        print_error_errno(_("realpath() failed for %s"), file_name);
        return 1;
    }

    return 0;
}

void __attribute__((nonnull))
output_close_file(const cotesdex_t * const cotesdex)
{
    if (cotesdex->output_file != stdout) {
        if (fclose(cotesdex->output_file)) {
            print_error_errno(_("could not close output file"));
        }
    }
    return;
}

void __attribute__((nonnull (1,2)))
output_vprint(const cotesdex_t * const cotesdex,
              const char * const fmt,
              va_list ap)
{
    if (cotesdex->output_comment) {
        fputs(comment_line(cotesdex->output_markup), cotesdex->output_file);
    }
    vfprintf(cotesdex->output_file, fmt, ap);
    return;
}

void __attribute__((format (printf, 2, 3), nonnull))
output_print(const cotesdex_t * const cotesdex, const char * fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    output_vprint(cotesdex, fmt, ap);
    va_end(ap);
    return;
}

static void __attribute__((nonnull))
output_print_buffer_line(const char * const line, void * const argument)
{
    const cotesdex_t * const cotesdex = argument;
    fputs(comment_line(cotesdex->output_markup), cotesdex->output_file);
    fputs(line, cotesdex->output_file);
    fputc('\n', cotesdex->output_file);
    return;
}

int __attribute__((nonnull))
output_print_buffer(const cotesdex_t * const cotesdex,
                    const char * const buffer)
{
    if (cotesdex->output_comment) {
        buffer_foreach_line(buffer, &output_print_buffer_line,
                            (void *)cotesdex);
    } else {
        fputs(buffer, cotesdex->output_file);
        fputc('\n', cotesdex->output_file);
    }
    return 0;
}

void __attribute__((nonnull))
output_begin_comment(cotesdex_t * const cotesdex)
{
    output_print(cotesdex, "%s\n",
                 comment_begin(cotesdex->output_markup));
    cotesdex->output_comment = true;
    return;
}

void __attribute__((nonnull))
output_end_comment(cotesdex_t * const cotesdex)
{
    cotesdex->output_comment = false;
    output_print(cotesdex, "%s\n",
                 comment_end(cotesdex->output_markup));
    return;
}
