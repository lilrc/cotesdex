/* 
 * This file is part of cotesdex.
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* print-error.c - error printing functions */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "io.h"
#include "print-error.h"

void __attribute__((nonnull))
print_error(const char * const fmt, ...)
{
    fputs(PACKAGE ": ", stderr);

    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    fputc('\n', stderr);

    return;
}

void __attribute__((nonnull))
print_error_errno(const char * const fmt, ...)
{
    fputs(PACKAGE ": ", stderr);

    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    /* Print the error with a lower-case leading character instead of upper-case
     * leading character. */
    const char * const error = strerror(errno);
    if (*error != '\0') {
        fputs(": ", stderr);
        fputc(tolower(*error), stderr);
        fputs(error + 1, stderr);
    }

    fputc('\n', stderr);

    return;
}
