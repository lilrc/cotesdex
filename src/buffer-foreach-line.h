/* 
 * This file is part of cotesdex.
 * 
 * Copyright (C) 2013-2015 Karl Linden <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* buffer-foreach-line.h - header for the buffer_foreach_line function */

#ifndef BUFFER_FOREACH_LINE_H
# define BUFFER_FOREACH_LINE_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* Helper function to split a buffer up into lines and call a function
 * for each of the lines. The line will be supplied as the first
 * argument to the function and the second argument is a user supplied
 * argument. */
void buffer_foreach_line(const char * const buffer,
                         void (*func)(const char *, void *),
                         void * argument)
    __attribute__((nonnull (1,2)));

#endif /* !BUFFER_FOREACH_LINE_H */
